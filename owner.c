#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void owner_area(user_t *u){
int choice;
do{
    printf("\n\n0.Sign out\n1. Appoint librarian\n2. edit profile\n3. change password\n4. fees/fine report\n5. Book Availability\n6. Book Categories/subjects\n");
    scanf("%d", &choice);
    switch (choice)
    {
    case 1:appoint_librarian();
        break;
    case 2:profile_edit_by_id(u->id);
        break;
    case 3:change_password(u);
        break;
    case 4:
        break;
    case 5:bookcopy_checkavail_details();
        break;
    case 6:
        break;}
    
} while (choice != 0);
}

void appoint_librarian(){
    user_t u;
    user_accept(&u);
    strcpy(u.role, ROLE_LIBRARIAN);
    user_add(&u);
}
