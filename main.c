#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"

void date_tester(){
	date_t d1={1,1,2000}, d2={31, 12, 2000};
	date_t d ={1,1,2000};
	date_t r= date_add(d, 366);
	date_print(&r);
	int diff = date_cmp(d1, d2);
	printf("date diff:%d\n", diff);
}

void tester(){
	user_t u;
	book_t b;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
}
void sign_in(){
	char email[30],password[10];
	user_t u;
	int invalid_user =0;
	printf("email:");
	scanf("%s",email);
	printf("password:");
	scanf("%s", password);
	if(user_find_by_email(&u, email) == 1) {
	if(strcmp(password, u.password)==0){
		if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);
		if(strcmp(u.role, ROLE_OWNER)==0)
		owner_area(&u);
		else if(strcmp(u.role, ROLE_LIBRARIAN)==0)
		librarian_area(&u);
		else if(strcmp(u.role, ROLE_MEMBER)==0)
		member_area(&u);
		else
			invalid_user =1;
	}
	else
			invalid_user =1;
			
	}
	else
			invalid_user =1;	
	
	if (invalid_user)
		printf("invalid email,password or role.\n");


}
void sign_up(){
	user_t u;
	user_accept(&u);
	user_add(&u);
}

	int main(){
		//printf("Hello Library\n");
		//tester();
		// int id = get_next_user_id();
		// printf("new id: %d\n",id);
	int choice;
		do
		{
			
		printf("\n\n0. exit \n1. sign in\n2. sign up\nEnter Choice:");
		scanf("%d", &choice);
	
    switch (choice)
    {
    case 1:   	// sign in
		sign_in();
        break;
    case 2:		// sign up
		sign_up();
        break;
	}
		}while(choice !=0);

		return 0;
	}
