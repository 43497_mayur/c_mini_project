#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void member_area(user_t *u) {
int choice;
char name[80];
do
{
    printf("\n\n0. Sign out\n1. find book\n2. edit profile\n3. change password\n4. Book Availability\n5.issued book\n");
    scanf("%d", &choice);
    switch (choice)
    {
    case 1:printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
        break;
    case 2:profile_edit_by_id(u->id);
        break;
    case 3:change_password(u);
        break;
    case 4:bookcopy_checkavail();
        break;
	case 5:display_issued_bookcopies(u->id);
        break;
   
    }
} while (choice != 0);
}

void bookcopy_checkavail(){
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	printf("enter the book id: ");
	scanf("%d",&book_id);
	fp = fopen(BOOKCOPY_DB,"rb");
	if(fp == NULL){
		perror("cannot open bookcopies file.");
		return;
	}
	while(fread(&bc, sizeof(bookcopy_t),1, fp)>0){
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0){
			//bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	printf("number of copies available: %d\n",count);
}




void profile_edit_by_id(user_t *u){
	int id, found = 0;
	FILE *fp;
	user_t p;
	printf("enter id: ");
	scanf("%d",&id);
	fp=fopen(USER_DB,"rb+");
	if(fp == NULL){
		perror("cannot open profile");
		exit(1);
	}
	while (fread(&p, sizeof(user_t),1,fp)>0)
	{
		if(id == p.id){
			found = 1;
			break;
		}
	}
	if(found){
		long size =sizeof(user_t);
		user_t nu;
		user_accept(&nu);
		nu.id=p.id;
		strcpy(nu.role,p.role);
		fseek(fp,-size,SEEK_CUR);
		fwrite(&nu, size,1 ,fp);
		printf("profile updated.\n");
	}
	else
		printf("profile not found.\n");
	fclose(fp);
}




void change_password(user_t *u){
	int id, found = 0;
	FILE *fp;
	fp=fopen(USER_DB,"rb+");
	if(fp == NULL){
		perror("cannot open profile");
		exit(1);
	}
		long size =sizeof(user_t);
		char p1[10];
		char p2[10];
		printf("\nEnter new password:=>");
		scanf("%s", p1);
		printf("\nEnter confirm password:=>");
		scanf("%s", p2);
		if( strcmp(p1, p2) == 0) {
			strcpy(u -> password, p1);
			printf("\n%s", u -> password);
			fseek(fp,-size,SEEK_CUR);
			fwrite(u, size, 1 ,fp);
			
			printf("\n Password has been changed successfully !");
		} else {
			printf("\n new password and confirm password should be same !");
		}
	fclose(fp);
}
